# This file is part of Velocity Docker.
#
# Velocity Docker is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or any later version.
#
# Velocity Docker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Velocity Docker. If not, see <https://www.gnu.org/licenses/>.

ARG VELOCITY_SRC="/usr/src/velocity"
ARG VELOCITY_VERSION="3.1.1"

FROM alpine:3.18.4 AS Base
LABEL org.opencontainers.image.authors="ultraxime@yahoo.com"

RUN apk --no-cache --update add \
        curl \
        bash

ARG VELOCITY_VERSION
ARG VELOCITY_SRC

SHELL ["/bin/bash", "-c" ]

RUN echo "Fetching Velocity" \
    && mkdir -p "$VELOCITY_SRC" \
    && BUILD=$(curl "https://api.papermc.io/v2/projects/velocity/versions/$VELOCITY_VERSION/" | sed -e 's/[^,0-9]//g' -e 's/[0-9]*,//g') \
    && curl "https://api.papermc.io/v2/projects/velocity/versions/$VELOCITY_VERSION/builds/$BUILD/downloads/velocity-$VELOCITY_VERSION-$BUILD.jar" --output "$VELOCITY_SRC/velocity-$VELOCITY_VERSION.jar"


FROM eclipse-temurin:11-jre-alpine
LABEL org.opencontainers.image.authors="ultraxime@yahoo.com"

RUN apk --no-cache --update add \
        bash \
        pwgen \
        rsync \
        tmux \
        xterm

ARG VELOCITY_SRC
ENV VELOCITY_HOME="/opt/velocity"
ENV VELOCITY_SRC="$VELOCITY_SRC"
ENV PLUGINS_DIR="$VELOCITY_HOME/plugins"

RUN addgroup -S user \
    && adduser -G user -S user -H -s /bin/bash \
    && mkdir -p "$VELOCITY_HOME" "$VELOCITY_SRC" "$PLUGINS_DIR" \
    && chown -R user:user "$VELOCITY_HOME" "$VELOCITY_SRC" "$PLUGINS_DIR"

USER user

SHELL ["/bin/bash", "-c" ]

RUN touch "$VELOCITY_SRC/first_time"

ARG SCRIPT="velocity"

COPY --chown=user:user --chmod=544 $SCRIPT /usr/local/bin/
ARG VELOCITY_VERSION
COPY --from=Base "$VELOCITY_SRC/velocity-$VELOCITY_VERSION.jar" "$VELOCITY_SRC/"

EXPOSE 25577


VOLUME [ "$VELOCITY_HOME", "$PLUGINS_DIR" ]

ENTRYPOINT ["velocity"]
CMD ["run"]

HEALTHCHECK \
    --timeout=2s \
    --start-period=20s \
    CMD ["velocity", "healthcheck"]
