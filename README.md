| ![gplv3-or-later](https://www.gnu.org/graphics/gplv3-or-later.png) | [![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit) |
|--------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------|

# Velocity Docker

## Description

This project provid a docker image for Velocity, a proxy server to run Minecraft Servers

## Base Docker image

* eclipse-temurin:11-jre-alpine

## How to use this image

### Building the image

```bash
docker build \
    -t ultraxime/velocity \
   .
```

When no option are specified, the image will build the 3.1.1 version of velocity.</br>
To change this behavior, you should specified the option `--build-arg VELOCITY_VERSION=<version>` when building the image.

#### Adding custom files

If you feel the need to add files in the `/opt/velocity` folder at build time, you can add the line: `COPY --chown=user:user --chmod=544 <files> "$VELOCITY_HOME/"`at the end of the dockerfile.

### Starting an instance

```bash
docker run \
    --name velocity-instance \
    -p 0.0.0.0:25577:25577 \
    -d \
    -it \
    ultraxime/velocity
```

This image exposes the standard minecraft proxy port (25577).

It is highly preferred to start the container with `-it`. This is needed in
order to allow executing console commands via `docker exec`. This also allows
Velocity to safely shutdown when stopping the container via `docker stop`. See
the `Scripting` section for more details.

#### Commands

The image uses an entrypoint script called `velocity`, which allows you to
execute preset commands. Should you attempt to execute an unrecognized command,
it will treat it as a regular shell command.

The commands are as follows:

* `run` - This runs the Velocity server, and is the default command used by the
  container. This command can accept additional parameters. Useful when
  creating a new container via `docker create` or `docker run`
* `console` - This executes a console command. This allows system administrators
  to perform complex tasks via scripts. This feature is off by default. See the
  `Scripting` section for more details and examples.
* `stop` - This performs a safe shutdown of the Velocity server, equivalent to
  `console end`
* `healthcheck` - This check the health status of the server. The check in
  performed by running the `velocity version` inside the server and check that
  it is answering `Velocity $VELOCITY_VERSION` in less than 2 seconds.

Here are some examples on how to use these commands:

#### Scripting

Unlike other Velocity Docker Images, this image provides a way to execute console
commands without attaching to the docker container. It lets system
administrators perform much more complex tasks, such as managing the docker
container from another docker container (e.g. deploying using Jenkins).

For those who are used to running `docker attach` inside a `screen` or `tmux`
session for scripting, this is going to be heaven.

This feature can be enabled by pasing the `-it` parameter to `docker create` or
`docker run`, which enables STDIN and TTY. This runs the Velocity server inside a
`tmux` session. This also enables safe shutdown mode when the container is
stopped.

Once enabled, you may now execute console commands like so:

```bash
docker exec velocity-instance velocity console say hello everybody!
```

Some warnings when using this feature:

* **Be careful when attaching to the console via `docker attach`**. You are
  attaching to a `tmux` session running on the foreground with the footer
  disabled. Do not try to detach from the `tmux` session using `CTRL-b d`,
  otherwise this will stop the container. To detach from the container, use
  `CTRL-p CTRL-q`, which is the standard escape sequence for `docker attach`.

Here is an example on how to notify players that the server will be shutdown
after 60 seconds:

```bash
#!/bin/bash
docker exec velocity-instance velocity console say We will be shutting down the server in 60s!
docker exec velocity-instance velocity console say Stop whatever you are doing!
sleep 60
docker exec velocity-instance velocity console say We will be back in 1 hour!
sleep 5

# The container will send the stop console command to the server for you, to
# ensure that the server is shutdown safely.
#
# Of course you can run this manually like so:
#
#     docker exec velocity-instance velocity console stop
#
# But this will restart the container if the restart policy is set to always.
docker stop -t 60 velocity-instance
```

## Custom jar file

You can use a custom jar file by inserting it in `/opt/velocity`,
either by using a volume, coping it during build time ...

* If the name of the jar file is of the shape `velocity-<version>.jar` it will be
  automatically detected and used instead of the default one.
* If it is not the case you can specify its name in the environment variable
  `VELOCITY_JAR`, be carefull in this case if you are not running 3.1.1, the healthcheck will fail.

## Custom Configuration

This image provids many way of altering its configuration, by using:

* Environment Variables
* `server-loader` a software designed to load plugins onto server, especialy Minecraft focused ones
* Docker volumes

### Environment Variables

This image uses environements variables to configure the JVM settings and the
`velocity.toml`.

#### VELOCITY_OPTS

You may adjust the JVM settings via the `VELOCITY_OPTS` variable.

<!--#### PLUGINS_DIR

You may adjust where the plugins and world are stored via the respective variable.

* **Be careful, it seems that velocity does no support yet moving the plugin folder**.
  So this option is not supported yet
-->

#### Environment variables for velocity.toml

Each entry in the `velocity.toml` file can be changed by passing the
appropriate variable. To make it easier to remember and configure, the variable
representation of each entry is in uppercase, and uses underscore instead
of dash.

The server port cannot be changed. This has to be remapped when starting an
instance.

For reference, here is the list of environment variables for `velocity.toml`
that you can set. The description are directly the one from Velocity, so 'we'
stands for Velocity server.

* `MOTD` (default:`&#09add3A Velocity Server`) : This gets displayed when the
  player adds your server to their server list. Legacy color codes and JSON
  are accepted.
* `SHOW_MAX_PLAYER` (default: `500`) : What should we display for the maximum
  number of players? (Velocity does not support a cap on the number of players
  online.)
* `ONLINE_MODE` (default: `true`) : Should we authenticate players with Mojang?
* `PREVENT_CLIENT_PROXY_CONNECTIONS` (default: `true`) : If client's ISP/AS
  sent from this proxy is different from the one from Mojang's authentication
  server, the player is kicked. This disallows some VPN and proxy connections
  but is a weak form of protection.
  **Default differs from the Velocity default** (Velocity default: `false`)
* `PLAYER_IP_FORWARDING_MODE` (default: `modern`) : Should we forward IP
  addresses and other data to backend servers? Available options:
  * `none`: No forwarding will be done. All players will appear to be
    connecting from the proxy and will have offline-mode UUIDs.
  * `legacy`: Forward player IPs and UUIDs in a BungeeCord-compatible format.
    Use this if you run servers using Minecraft 1.12 or lower.
  * `bungeeguard`: Forward player IPs and UUIDs in a format supported by the
    BungeeGuard plugin. Use this if you run servers using Minecraft 1.12 or
    lower, and are unable to implement network level firewalling (on a shared
    host).
  * `modern`: Forward player IPs and UUIDs as part of the login process using
    Velocity's native forwarding. Only applicable for Minecraft 1.13 or higher.

  **Default differs from the Velocity default** (Velocity default: `none`)
* `ANNOUNCE_FORGE` (default: `false`) : Announce whether or not your server
  supports Forge. If you run a modded server, we suggest turning this on. </br>
  If your network runs one modpack consistently, consider using
  ping-passthrough = "mods" instead for a nicer display in the server list.
* `KICK_EXISTING_PLAYERS` (default: `false`) : If enabled and the proxy is in
  online mode, Velocity will kick any existing player who is online if a
  duplicate connection attempt is made.
* `PING_PASSTHROUGH` (default: `disabled`) : Should Velocity pass server list
  ping requests to a backend server? Available options:
  * `disabled`: No pass-through will be done. The velocity.toml and
    server-icon.png will determine the initial server list ping response.
  * `mods`: Passes only the mod list from your backend server into the
    response. The first server in your try list (or forced host) with a mod
    list will be used. If no backend servers can be contacted, Velocity won't
    display any mod information.
  * `description`: Uses the description and mod list from the backend server.
    The first server in the try (or forced host) list that responds is used for
    the description and mod list.
  * `all`: Uses the backend server's response as the proxy response. The
    Velocity configuration is used if no servers could be contacted.
* `COMPRESSION_THRESHOLD` (default: `256`) : How large a Minecraft packet has
  to be before we compress it. Setting this to zero will compress all packets,
  and setting it to -1 will disable compression entirely.
* `COMPRESSION_LEVEL` (default: `-1`) : How much compression should be done
  (from 0-9). The default is -1, which uses the default level of 6.
* `LOGIN_RATE` (default: `3000`) : How fast (in milliseconds) are clients
  allowed to connect after the last connection? Disable this by setting this to 0.
* `CONNECTION_TIMEOUT` (default: `5000`) : Specify a custom timeout for
  connection timeouts here.
* `READ_TIMEOUT` (default:`30000`) : Specify a read timeout for connections here.
* `HAPROXY_PROTOCOL` (default: `false`) : Enables compatibility with HAProxy's
  PROXY protocol. If you don't know what this is for, then don't enable it.
* `TCP_FAST_OPEN` (default: `true`) : Enables TCP fast open support on the
  proxy. Requires the proxy to run on Linux.
  **Default differs from the Velocity default** (Velocity default: `false`)
* `BUNGEE_PLUGIN_MESSAGE_CHANNEL` (default: `true`) : Enables BungeeCord plugin
  messaging channel support on Velocity.
* `SHOW_PING_REQUESTS` (default: `false`) : Shows ping requests to the proxy
  from clients.
* `FAILOVER_ON_UNEXPECTED_SERVER_DISCONNECT` (default: `true`) : By default,
  Velocity will attempt to gracefully handle situations where the user
  unexpectedly loses connection to the server without an explicit disconnect
  message by attempting to fall the user back, except in the case of read
  timeouts. BungeeCord will disconnect the user instead. You can disable this
  setting to use the BungeeCord behavior.
* `ANNOUNCE_PROXY_COMMANDS` (default: `true`) : Declares the proxy commands to
  1.13+ clients.
* `LOG_COMMAND_EXECUTIONS` (default: `true`) : Enables the logging of commands
  **Default differs from the Velocity default** (Velocity default: `false`)
* `QUERY_ENABLE` (default: `false`) : Whether to enable responding to GameSpy
  4 query responses or not.
* `QUERY_PORT` (default: `25577`) : If query is enabled, on what port should
  the query protocol listen on?
* `QUERY_MAP` (default: `Velocity`) : This is the map name that is reported to
  the query services.
* `QUERY_SHOW_PLUGINS` (default: `false`)

For the `servers` and `forced-hosts` the choice have been made to use files
instead of environment variables.

Then if the corresponding file (`/opt/velocity/<section>.toml`) exists its
content will be past instead of the default value

* `servers`: Configure your servers here. Each key represents the server's
  name, and the value represents the IP address of the server to connect to.
  default:

  ```
  lobby = "127.0.0.1:30066"
  factions = "127.0.0.1:30067"
  minigames = "127.0.0.1:30068"
  # In what order we should try servers when a player logs in or is kicked from a server.
  try = ["lobby"]
  ```

* `forced-hosts`: default:

  ```
  "lobby.example.com" = ["lobby"]
  "factions.example.com" = ["factions"]
  "minigames.example.com" = ["minigames"]
  ```

### Server-loader

Server-loader is a software developed by [Ultraxime](https://gitlab.com/ultraxime).
It is designed to help fetching plugins and install them when running a server.
If the file `/opt/velocity/server-loader.yml` exists, the plugin folder will be
emptied of all jar files. Then following the instruction contained in the file
it will fetch the plugins.

This is an example of `server-loader.yml` fetching WorldEdit:

```yaml
plugins:
  - name: AsyncWorldEdit
    url: "https://github.com/SBPrime/releases/download/v3.9.4/AsyncWorldEdit-3.9.4.jar"
    version: 3.9.4
```

For more information look the [GitLab repository](https://gitlab.com/e4333/docker-images/server-loader).

### Data volumes

Be careful when mounting volumes, the permissions are not updated.

There are two data volumes declared for this image:

#### /opt/velocity

All server-related artifacts (jars, configs)' go here.

#### /opt/velocity/plugins

This contains the plugins and their configuration files.

The recommended approach to handling plugin data is to use a separate data
volume container. You can create one with the following command:

```bash
docker run --name velocity-plugin -v /opt/velocity/plugins eclipse-temurin:11-jre-alpine true
```

## Extending this image

This image is meant to be extended for packaging plugins, and
configurations as Docker images. For server owners, this is the best way to
roll out configuration changes and updates to your servers.

## Supported Docker versions

This image has been tested on Docker version 1.41 on amd64

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
